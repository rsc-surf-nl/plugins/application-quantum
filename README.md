# Quantum computing for SURF Research Cloud

## Description

This is an Ansible playbook that will install and configure the SURF Quantum library for Python on a compute workspace. The purpose is to make it easy to work with quantum computing providers through public cloud, without having to work out the infrastructure configuration and the authentication as an end-user.

The targeted infrastructure is [SURF Research Cloud](https://www.surf.nl/en/surf-research-cloud-collaboration-portal-for-research).

## Installation

Research Cloud is wrapping Ansible Playbooks into "plugins" which are run by the Research Cloud deployer to create a particular "turn-key" virtual resource.
To use this code in the intended way, you'd need access to SURF Research Cloud and just start the "Quantum" application.

[Research Cloud user docs](https://servicedesk.surf.nl/wiki/x/HIKV)

The required parameters for this script are:

 * `azure_quantum_workspace_id`; The Azure ID of the Azure Quantum Workspace
 * `azure_quantum_location`; The location where the Azure Quantum Workspace is running (eg. "West Europe")
